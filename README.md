#Nom du Projet
###Test technique, il s'agir d'un import de fichier avec enregistrement des données en base de données.

#Prérequis :
```
PHP 7.4 ou supérieur
Serveur de base de données (par exemple : MySQL, PostgreSQL)
Composer
Node.js & Yarn (si vous utilisez Webpack Encore)
Tout autre prérequis spécifique à votre projet
```
#Installation
###1 - Cloner le dépôt :
```
git clone https://gitlab.com/89nyaina/etalik_maminiaina.git
```

###2 - Installer les dépendances PHP :
```
composer install
```
###3 - Configurer la base de données :
Ajustez la chaîne de connexion dans le fichier .env ou créez un fichier .env.local pour vos paramètres locaux.
```
DATABASE_URL="mysql://root@127.0.0.1:3306/eTalik?serverVersion=8&charset=utf8mb4"
```

###3 - Créez ensuite la base de données et les tables :
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

###4 - (Optionnel) Installer les dépendances front-end :
```
yarn install
```

###5 - Lancer le serveur de développement :
Vous pouvez utiliser le serveur web interne de PHP :
```
php bin/console server:run
```

Ou si vous utilisez Symfony CLI :
```
symfony server:start
```


#Utilisation
Notre application est une plateforme puissante qui facilite la gestion et l'importation des données. Elle offre une interface intuitive basée sur la technologie Twig de Symfony, permettant ainsi une interaction fluide et conviviale pour les utilisateurs.