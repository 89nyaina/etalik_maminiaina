<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230810131514 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE infos_vehicules_clients (id INT AUTO_INCREMENT NOT NULL, compte_affaire VARCHAR(100) NOT NULL, compte_evenement VARCHAR(100) NOT NULL, compte_dernier_evenement VARCHAR(100) NOT NULL, numero_fiche INT NOT NULL, civilit� VARCHAR(100) DEFAULT NULL, proprietaire_actuel VARCHAR(100) DEFAULT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) DEFAULT NULL, adresse_voie VARCHAR(100) DEFAULT NULL, complement_adresse VARCHAR(100) DEFAULT NULL, code_postal VARCHAR(10) NOT NULL, ville VARCHAR(50) NOT NULL, tel_domicile VARCHAR(15) DEFAULT NULL, tel_portable VARCHAR(15) DEFAULT NULL, tel_travail VARCHAR(15) DEFAULT NULL, email VARCHAR(100) NOT NULL, date_mise_circulation DATE DEFAULT NULL, date_achat DATE DEFAULT NULL, date_dernier_evenement DATE NOT NULL, marque VARCHAR(30) NOT NULL, modele VARCHAR(30) DEFAULT NULL, version_modele VARCHAR(100) DEFAULT NULL, vin VARCHAR(20) NOT NULL, immatriculation VARCHAR(15) DEFAULT NULL, type_prospect VARCHAR(30) NOT NULL, kilometrage INT DEFAULT NULL, type_energie VARCHAR(15) DEFAULT NULL, vendeur_vn VARCHAR(100) DEFAULT NULL, vendeur_vo VARCHAR(100) DEFAULT NULL, commentaire_facturation LONGTEXT DEFAULT NULL, type_transaction VARCHAR(2) DEFAULT NULL, numero_dossier_transaction VARCHAR(8) DEFAULT NULL, intermediaire_vente_vn VARCHAR(100) DEFAULT NULL, date_evenement DATE NOT NULL, origine_evenement VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE infos_vehicules_clients');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
