$(document).ready(function() {
    var table = $('#tableau').DataTable({
        dom: 'Bfrtip',
        buttons: [{
            extend: 'colvis',
            text: 'Sélectionner les colonnes'
        }],
        columnDefs: [{
            targets: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34],
            visible: false
        }],
        language: {
            paginate: {
                first:    "Premier",
                previous: "Précédent",
                next:     "Suivant",
                last:     "Dernier"
            },
            info: "Véhicules _START_ à _END_ sur _TOTAL_",
            infoFiltered: "(parmi _MAX_ enregistrés)",
            infoEmpty: "Aucun véhicule trouvé",
        }
     
    });

    var buttonWidth = $('.dt-button-collection').outerWidth();
    $('.dt-button-collection + .dt-button-collection').css('width', buttonWidth);

    const $flashModal = $("#flashMessageModal");
    const $overlay = $("#overlay");
    if ($flashModal.text().trim() !== "") {
        $overlay.show();
        $flashModal.show();
        setTimeout(function() {
            $overlay.hide();
            $flashModal.hide();
        }, 3000);
    }
});