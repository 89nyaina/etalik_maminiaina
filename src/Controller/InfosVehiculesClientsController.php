<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\ImportFileType;
use App\Service\InfosVehiculesClientsInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class InfosVehiculesClientsController extends AbstractController
{
    private InfosVehiculesClientsInterface $infosVehiculesClientsInterface;

    /**
     * Constructeur pour initialiser le service.
     *
     * @param InfosVehiculesClientsInterface $infosVehiculesClientsInterface
     */
    public function __construct(InfosVehiculesClientsInterface $infosVehiculesClientsInterface)
    {
        $this->infosVehiculesClientsInterface = $infosVehiculesClientsInterface;
    }

    /**
     * Retourne la page d'accueil avec le formulaire d'import et le tableau des véhicules.
     *
     * @return Response
     */
    public function index(): Response
    {
        // Crée le formulaire d'import.
        $form = $this->createForm(ImportFileType::class, null, [
            'action' => $this->generateUrl('import_file_route'),
        ]);

        // Renvoie la vue avec le formulaire et les données.
        return $this->render('infos_vehicules_clients/index.html.twig', [
            'form' => $form->createView(),
            'datas' => $this->infosVehiculesClientsInterface->show(),
        ]);
    }

    /**
     * Gère l'importation du fichier envoyé par l'utilisateur.
     *
     * @param Request $request La requête HTTP
     * @return Response
     */
    public function importeFile(Request $request): Response
    {
        $file = new File();

        // Associe le fichier à un formulaire.
        $form = $this->createForm(ImportFileType::class, $file);

        // Traite la requête.
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $file = $form->get('file')->getData();
                if ($file) {
                    $this->infosVehiculesClientsInterface->processFile($file->getPathname());
                }
            } else {
                // Gère les erreurs du formulaire.
                if ($form->getErrors(true)) {
                    $this->addFlash('error', $form->getErrors(true)[0]->getMessage());
                }
            }
        }

        return $this->redirectToRoute('index');
    }
}
