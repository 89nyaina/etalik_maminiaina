<?php

namespace App\Service;

use App\Controller\InfosVehiculesClientsController;
use App\Entity\InfosVehiculesClients;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\InfosVehiculesClientsInterface;
use App\Repository\InfosVehiculesClientsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Service pour gérer les informations des véhicules clients.
 */
class InfosVehiculesClientsService extends InfosVehiculesClientsController implements InfosVehiculesClientsInterface
{
    private $em;           // Gestionnaire d'entités Doctrine
    private $repo;         // Référentiel des informations des véhicules clients
    private $validator;    // Service de validation des entités

    /**
     * Constructeur.
     *
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     * @param InfosVehiculesClientsRepository $repo
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, InfosVehiculesClientsRepository $repo)
    {
        $this->em = $em;
        $this->repo = $repo;
        $this->validator = $validator;
    }

    /**
     * Récupère toutes les informations des véhicules clients.
     *
     * @return array
     */
    public function show(): array
    {
        return $this->repo->findAll();
    }
    
    /**
     * Traite le fichier fourni pour extraire et sauvegarder les informations des véhicules clients.
     *
     * @param string $filePath Chemin du fichier à traiter.
     * @return Response|null Réponse indiquant le succès ou l'échec de l'opération.
     */
    public function processFile(string $filePath): ?Response
    {
        // Charge le fichier dans un objet de tableau
        $spreadsheet = IOFactory::load($filePath);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();
        $headerIndices = [];
        $header = $rows[0];  // Les en-têtes du fichier

        // Parcourez chaque ligne du fichier (en commençant par la deuxième ligne)
        for ($i = 1; $i < count($rows); $i++) {
            $dataRow = $rows[$i];
            $InfosVehiculesClients = new InfosVehiculesClients();

            // Assigne les valeurs aux entités en fonction des en-têtes
            foreach ($header as $index => $columnName) {
                $headerIndices[$columnName] = $index;
            }
            if (isset($headerIndices['Compte Affaire'])) {
                $InfosVehiculesClients->setCompteAffaire($dataRow[$headerIndices['Compte Affaire']]);
            }
            if (isset($headerIndices['Compte évènement (Veh)'])) {
                $InfosVehiculesClients->setCompteEvenement($dataRow[$headerIndices['Compte évènement (Veh)']]);
            }
            if (isset($headerIndices['Compte dernier évènement (Veh)'])) {
                $InfosVehiculesClients->setCompteDernierEvenement($dataRow[$headerIndices['Compte dernier évènement (Veh)']]);
            }
            if (isset($headerIndices['Numéro de fiche'])) {
                $InfosVehiculesClients->setNumeroFiche($dataRow[$headerIndices['Numéro de fiche']]);
            }
            if (isset($headerIndices['Libellé civilité'])) {
                $InfosVehiculesClients->setCivilite($dataRow[$headerIndices['Libellé civilité']]);
            }
            if (isset($headerIndices['Propriétaire actuel du véhicule'])) {
                $InfosVehiculesClients->setProprietaireActuel($dataRow[$headerIndices['Propriétaire actuel du véhicule']]);
            }
            if (isset($headerIndices['Nom'])) {
                $InfosVehiculesClients->setNom($dataRow[$headerIndices['Nom']]);
            }
            if (isset($headerIndices['Prénom'])) {
                $InfosVehiculesClients->setPrenom($dataRow[$headerIndices['Prénom']]);
            }
            if (isset($headerIndices['N° et Nom de la voie'])) {
                $InfosVehiculesClients->setAdresseVoie($dataRow[$headerIndices['N° et Nom de la voie']]);
            }
            if (isset($headerIndices['Complément adresse 1'])) {
                $InfosVehiculesClients->setComplementAdresse($dataRow[$headerIndices['Complément adresse 1']]);
            }
            if (isset($headerIndices['Code postal'])) {
                $InfosVehiculesClients->setCodePostal($dataRow[$headerIndices['Code postal']]);
            }
            if (isset($headerIndices['Ville'])) {
                $InfosVehiculesClients->setVille($dataRow[$headerIndices['Ville']]);
            }
            if (isset($headerIndices['Téléphone domicile'])) {
                $InfosVehiculesClients->setTelDomicile($dataRow[$headerIndices['Téléphone domicile']]);
            }
            if (isset($headerIndices['Téléphone portable'])) {
                $InfosVehiculesClients->setTelPortable($dataRow[$headerIndices['Téléphone portable']]);
            }
            if (isset($headerIndices['Téléphone job'])) {
                $InfosVehiculesClients->setTelTravail($dataRow[$headerIndices['Téléphone job']]);
            }
            if (isset($headerIndices['Email'])) {
                $InfosVehiculesClients->setEmail($dataRow[$headerIndices['Email']]);
            }
            if (isset($headerIndices['Date de mise en circulation'])) {
                $InfosVehiculesClients->setDateMiseCirculation(new \DateTime($dataRow[$headerIndices['Date de mise en circulation']]));
            }
            if (isset($headerIndices['Date achat (date de livraison)'])) {
                $InfosVehiculesClients->setDateAchat(new \DateTime($dataRow[$headerIndices['Date achat (date de livraison)']]));
            }
            if (isset($headerIndices['Date dernier évènement (Veh)'])) {
                $InfosVehiculesClients->setDateDernierEvenement(new \DateTime($dataRow[$headerIndices['Date dernier évènement (Veh)']]));
            }
            if (isset($headerIndices['Libellé marque (Mrq)'])) {
                $InfosVehiculesClients->setMarque($dataRow[$headerIndices['Libellé marque (Mrq)']]);
            }
            if (isset($headerIndices['Libellé modèle (Mod)'])) {
                $InfosVehiculesClients->setModele($dataRow[$headerIndices['Libellé modèle (Mod)']]);
            }
            if (isset($headerIndices['Version'])) {
                $InfosVehiculesClients->setVersionModele($dataRow[$headerIndices['Version']]);
            }
            if (isset($headerIndices['VIN'])) {
                $InfosVehiculesClients->setVin($dataRow[$headerIndices['VIN']]);
            }
            if (isset($headerIndices['Immatriculation'])) {
                $InfosVehiculesClients->setImmatriculation($dataRow[$headerIndices['Immatriculation']]);
            }
            if (isset($headerIndices['Type de prospect'])) {
                $InfosVehiculesClients->setTypeProspect($dataRow[$headerIndices['Type de prospect']]);
            }
            if (isset($headerIndices['Kilométrage'])) {
                $InfosVehiculesClients->setKilometrage($dataRow[$headerIndices['Kilométrage']]);
            }
            if (isset($headerIndices['Libellé énergie (Energ)'])) {
                $InfosVehiculesClients->setTypeEnergie($dataRow[$headerIndices['Libellé énergie (Energ)']]);
            }
            if (isset($headerIndices['Vendeur VN'])) {
                $InfosVehiculesClients->setVendeurVn($dataRow[$headerIndices['Vendeur VN']]);
            }
            if (isset($headerIndices['Vendeur VO'])) {
                $InfosVehiculesClients->setVendeurVo($dataRow[$headerIndices['Vendeur VO']]);
            }
            if (isset($headerIndices['Commentaire de facturation (Veh)'])) {
                $InfosVehiculesClients->setCommentaireFacturation($dataRow[$headerIndices['Commentaire de facturation (Veh)']]);
            }
            if (isset($headerIndices['Type VN VO'])) {
                $InfosVehiculesClients->setTypeTransaction($dataRow[$headerIndices['Type VN VO']]);
            }
            if (isset($headerIndices['Numéro de dossier VN VO'])) {
                $InfosVehiculesClients->setNumeroDossierTransaction($dataRow[$headerIndices['Numéro de dossier VN VO']]);
            }
            if (isset($headerIndices['Intermediaire de vente VN'])) {
                $InfosVehiculesClients->setIntermediaireVenteVn($dataRow[$headerIndices['Intermediaire de vente VN']]);
            }
            if (isset($headerIndices['Date évènement (Veh)'])) {
                $InfosVehiculesClients->setDateEvenement(new \DateTime($dataRow[$headerIndices['Date évènement (Veh)']]));
            }
            if (isset($headerIndices['Origine évènement (Veh)']) and $dataRow[$headerIndices['Origine évènement (Veh)']] != NULL) {
                $InfosVehiculesClients->setOrigineEvenement($dataRow[$headerIndices['Origine évènement (Veh)']]);
            }
            // ... (le code reste le même pour affecter les valeurs)

            // Valide l'entité avant la persistance
            $errors = $this->validator->validate($InfosVehiculesClients);
            
            if (count($errors) > 0) {
                // Retourne une erreur si la validation échoue
                return $this->addFlash('error', $errors[0]->getMessage());
            }
            $this->em->persist($InfosVehiculesClients);
        }

        // Enregistre les données dans la base de données
        $this->em->flush();

        return $this->addFlash('success', 'Fichier importé avec succès.');
    }
}
