<?php

namespace App\Service;

/**
 * Interface définissant les méthodes pour la gestion des informations des véhicules clients.
 */
interface InfosVehiculesClientsInterface
{
    /**
     * Récupère l'ensemble des informations des véhicules clients.
     *
     * @return mixed Les informations des véhicules clients.
     */
    public function show();

    /**
     * Traite l'import d'un fichier spécifié pour extraire les informations des véhicules clients.
     *
     * @param string $filePath Chemin du fichier à importer.
     * @return mixed Résultat de l'importation du fichier.
     */
    public function processFile(string $filePath);
}
