<?php

namespace App\Repository;

use App\Entity\InfosVehiculesClients;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InfosVehiculesClients>
 *
 * @method InfosVehiculesClients|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfosVehiculesClients|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfosVehiculesClients[]    findAll()
 * @method InfosVehiculesClients[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfosVehiculesClientsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfosVehiculesClients::class);
    }

    public function add(InfosVehiculesClients $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(InfosVehiculesClients $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return InfosVehiculesClients[] Returns an array of InfosVehiculesClients objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?InfosVehiculesClients
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
