<?php 

namespace App\Entity;

/**
 * Trait permettant de gérer l'ID des entités.
 */
trait ResourceId
{
    /**
     * Identifiant unique de l'entité.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;
    
    /**
     * Récupère l'identifiant de l'entité.
     *
     * @return int|null L'ID si défini, sinon null.
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
