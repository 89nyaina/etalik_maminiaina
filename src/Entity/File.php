<?php

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class File
{
    // Cette classe représente une entité de fichier.

    /**
     * Cette propriété stocke le fichier qui est téléchargé.
     * 
     * @var UploadedFile|null
     * 
     * @Assert\File(
     *     mimeTypes={"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
     *     mimeTypesMessage="Veuillez télécharger un fichier valide (.xlsx)."
     * )
     */
    private $file;

    /**
     * Récupère le fichier téléchargé.
     * 
     * @return UploadedFile|null Le fichier téléchargé ou null s'il n'y en a pas.
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * Définit le fichier téléchargé pour cette entité.
     * 
     * @param UploadedFile|null $file Le fichier à définir.
     * @return self Retourne l'instance de l'objet pour permettre les appels chaînés.
     */
    public function setFile(?UploadedFile $file): self
    {
        $this->file = $file;
        return $this;
    }
}

