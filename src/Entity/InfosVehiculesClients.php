<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InfosVehiculesClientsRepository::class)
 */
class InfosVehiculesClients
{
    use ResourceId;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Veuillez renseigner le Compte affaire.")
     * @Assert\Length(max=100, maxMessage="Le Compte affaire ne doit pas dépasser {{ limit }} caractères.")
     */
    private $compte_affaire;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Veuillez renseigner le Compte événement.")
     * @Assert\Length(max=100, maxMessage="Le Compte événement ne doit pas dépasser {{ limit }} caractères.")
     */
    private $compte_evenement;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Veuillez renseigner le Compte du dernier événement.")
     * @Assert\Length(max=100, maxMessage="Le Compte du dernier évènement ne doit pas dépasser {{ limit }} caractères.")
     */
    private $compte_dernier_evenement;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Veuillez renseigner le Numéro de fiche.")
     */
    private $numero_fiche;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="La Civilité ne doit pas dépasser {{ limit }} caractères.")
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="Le Propriétaire actuel ne doit pas dépasser {{ limit }} caractères.")
     */
    private $proprietaire_actuel;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Veuillez renseigner le Nom.")
     * @Assert\Length(max=50, maxMessage="Le Nom ne doit pas dépasser {{ limit }} caractères.")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Length(max=50, maxMessage="Le Prénom ne doit pas dépasser {{ limit }} caractères.")
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="L'Adresse voie ne doit pas dépasser {{ limit }} caractères.")
     */
    private $adresse_voie;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="Le champ Complément adresse ne doit pas dépasser 100 caractères.")
     */
    private $complement_adresse;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="Veuillez renseigner le Code postal.")
     * @Assert\Length(max=10, maxMessage="Le Code postal ne doit pas dépasser {{ limit }} caractères.")
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Veuillez renseigner la Ville.")
     * @Assert\Length(max=50, maxMessage="La Ville ne doit pas dépasser {{ limit }} caractères.")
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max=15, maxMessage="Le Tel domicile ne doit pas dépasser {{ limit }} caractères.")
     */
    private $tel_domicile;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max=15, maxMessage="Le Tel portable ne doit pas dépasser {{ limit }} caractères.")
     */
    private $tel_portable;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max=15, maxMessage="Le Tel travail ne doit pas dépasser {{ limit }} caractères.")
     */
    private $tel_travail;

    /**
     * @ORM\Column(type="string", length=100 , nullable=true))
     * @Assert\Email(message="Le format de l'Email est invalide.")
     * @Assert\Length(max=100, maxMessage="L'Email ne doit pas dépasser {{ limit }} caractères.")
     */
    private $email;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_mise_circulation;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_achat;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Veuillez renseigner la Date du dernier événement.")
     */
    private $date_dernier_evenement;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Veuillez renseigner la Marque.")
     * @Assert\Length(max=30, maxMessage="La Marque ne doit pas dépasser {{ limit }} caractères.")
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Assert\Length(max=30, maxMessage="Le Modèle ne doit pas dépasser {{ limit }} caractères.")
     */
    private $modele;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="La Version modèle ne doit pas dépasser {{ limit }} caractères.")
     */
    private $version_modele;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="Veuillez renseigner le VIN.")
     * @Assert\Length(max=20, maxMessage="Le VIN ne doit pas dépasser {{ limit }} caractères.")
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max=15, maxMessage="L'Immatriculation ne doit pas dépasser {{ limit }} caractères.")
     */
    private $immatriculation;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Veuillez renseigner le Type de prospect.")
     * @Assert\Length(max=30, maxMessage="Le Type de prospect ne doit pas dépasser {{ limit }} caractères.")
     */
    private $type_prospect;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\PositiveOrZero(message="Le kilométrage doit être un nombre positif ou zéro.")
     */
    private $kilometrage;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max=15, maxMessage="Le Type d'énergie ne doit pas dépasser {{ limit }} caractères.")
     */
    private $type_energie;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="Le Vendeur VN ne doit pas dépasser {{ limit }} caractères.")
     */
    private $vendeur_vn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="Le Vendeur VO ne doit pas dépasser {{ limit }} caractères.")
     */
    private $vendeur_vo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaire_facturation;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     * @Assert\Length(max=2, maxMessage="Le Type de transaction ne doit pas dépasser {{ limit }} caractères.")
     */
    private $type_transaction;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     * @Assert\Length(max=8, maxMessage="Le Numéro de dossier de transaction ne doit pas dépasser {{ limit }} caractères.")
     */
    private $numero_dossier_transaction;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="L'Intermédiaire de vente VN ne doit pas dépasser {{ limit }} caractères.")
     */
    private $intermediaire_vente_vn;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Veuillez renseigner la Date d'événement.")
     */
    private $date_evenement;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Veuillez renseigner l'Origine d'événement.")
     * @Assert\Length(max=30, maxMessage="L'Origine d'événement ne doit pas dépasser {{ limit }} caractères.")
     */
    private $origine_evenement; 


    public function getCompteAffaire(): ?string
    {
        return $this->compte_affaire;
    }

    public function setCompteAffaire(string $compte_affaire): self
    {
        $this->compte_affaire = $compte_affaire;

        return $this;
    }

    public function getCompteEvenement(): ?string
    {
        return $this->compte_evenement;
    }

    public function setCompteEvenement(?string $compte_evenement): self
    {
        $this->compte_evenement = $compte_evenement;

        return $this;
    }

    public function getCompteDernierEvenement(): ?string
    {
        return $this->compte_dernier_evenement;
    }

    public function setCompteDernierEvenement(?string $compte_dernier_evenement): self
    {
        $this->compte_dernier_evenement = $compte_dernier_evenement;

        return $this;
    }

    public function getNumeroFiche(): ?int
    {
        return $this->numero_fiche;
    }

    public function setNumeroFiche(?int $numero_fiche): self
    {
        $this->numero_fiche = $numero_fiche;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getProprietaireActuel(): ?string
    {
        return $this->proprietaire_actuel;
    }

    public function setProprietaireActuel(?string $proprietaire_actuel): self
    {
        $this->proprietaire_actuel = $proprietaire_actuel;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresseVoie(): ?string
    {
        return $this->adresse_voie;
    }

    public function setAdresseVoie(?string $adresse_voie): self
    {
        $this->adresse_voie = $adresse_voie;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complement_adresse;
    }

    public function setComplementAdresse(?string $complement_adresse): self
    {
        $this->complement_adresse = $complement_adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelDomicile(): ?string
    {
        return $this->tel_domicile;
    }

    public function setTelDomicile(?string $tel_domicile): self
    {
        $this->tel_domicile = $tel_domicile;

        return $this;
    }

    public function getTelPortable(): ?string
    {
        return $this->tel_portable;
    }

    public function setTelPortable(?string $tel_portable): self
    {
        $this->tel_portable = $tel_portable;

        return $this;
    }

    public function getTelTravail(): ?string
    {
        return $this->tel_travail;
    }

    public function setTelTravail(?string $tel_travail): self
    {
        $this->tel_travail = $tel_travail;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateMiseCirculation(): ?\DateTimeInterface
    {
        return $this->date_mise_circulation;
    }

    public function setDateMiseCirculation(?\DateTimeInterface $date_mise_circulation): self
    {
        $this->date_mise_circulation = $date_mise_circulation;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->date_achat;
    }

    public function setDateAchat(?\DateTimeInterface $date_achat): self
    {
        $this->date_achat = $date_achat;

        return $this;
    }

    public function getDateDernierEvenement(): ?\DateTimeInterface
    {
        return $this->date_dernier_evenement;
    }

    public function setDateDernierEvenement(?\DateTimeInterface $date_dernier_evenement): self
    {
        $this->date_dernier_evenement = $date_dernier_evenement;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(?string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(?string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getVersionModele(): ?string
    {
        return $this->version_modele;
    }

    public function setVersionModele(?string $version_modele): self
    {
        $this->version_modele = $version_modele;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(?string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getTypeProspect(): ?string
    {
        return $this->type_prospect;
    }

    public function setTypeProspect(?string $type_prospect): self
    {
        $this->type_prospect = $type_prospect;

        return $this;
    }

    public function getKilometrage(): ?int
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?int $kilometrage): self
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getTypeEnergie(): ?string
    {
        return $this->type_energie;
    }

    public function setTypeEnergie(?string $type_energie): self
    {
        $this->type_energie = $type_energie;

        return $this;
    }

    public function getVendeurVn(): ?string
    {
        return $this->vendeur_vn;
    }

    public function setVendeurVn(?string $vendeur_vn): self
    {
        $this->vendeur_vn = $vendeur_vn;

        return $this;
    }

    public function getVendeurVo(): ?string
    {
        return $this->vendeur_vo;
    }

    public function setVendeurVo(?string $vendeur_vo): self
    {
        $this->vendeur_vo = $vendeur_vo;

        return $this;
    }

    public function getCommentaireFacturation(): ?string
    {
        return $this->commentaire_facturation;
    }

    public function setCommentaireFacturation(?string $commentaire_facturation): self
    {
        $this->commentaire_facturation = $commentaire_facturation;

        return $this;
    }

    public function getTypeTransaction(): ?string
    {
        return $this->type_transaction;
    }

    public function setTypeTransaction(?string $type_transaction): self
    {
        $this->type_transaction = $type_transaction;

        return $this;
    }

    public function getNumeroDossierTransaction(): ?string
    {
        return $this->numero_dossier_transaction;
    }

    public function setNumeroDossierTransaction(?string $numero_dossier_transaction): self
    {
        $this->numero_dossier_transaction = $numero_dossier_transaction;

        return $this;
    }

    public function getIntermediaireVenteVn(): ?string
    {
        return $this->intermediaire_vente_vn;
    }

    public function setIntermediaireVenteVn(?string $intermediaire_vente_vn): self
    {
        $this->intermediaire_vente_vn = $intermediaire_vente_vn;

        return $this;
    }

    public function getDateEvenement(): ?\DateTimeInterface
    {
        return $this->date_evenement;
    }

    public function setDateEvenement(\DateTimeInterface $date_evenement): self
    {
        $this->date_evenement = $date_evenement;

        return $this;
    }

    public function getOrigineEvenement(): ?string
    {
        return $this->origine_evenement;
    }

    public function setOrigineEvenement(string $origine_evenement): self
    {
        $this->origine_evenement = $origine_evenement;

        return $this;
    }
}
