<?php 
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ImportFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Ajoutez un champ pour l'importation du fichier
            ->add('file', FileType::class, [
                'label' => 'Fichier à importer (.xlsx) *', // Étiquette du champ
                'required' => true, // Le champ est obligatoire
                // Ajoutez des contraintes pour vérifier le fichier
                'constraints' => [
                    new File([
                        'maxSize' => '5M',  // Limite la taille du fichier à 5Mo. Vous pouvez ajuster selon vos besoins.
                        'mimeTypes' => [
                            // Seuls les fichiers .xlsx sont acceptés
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        ],
                        'mimeTypesMessage' => 'Veuillez télécharger un fichier valide (.xlsx)', // Message en cas d'erreur
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Configure les options par défaut. Pour le moment, nous n'en avons pas besoin, alors nous laissons un tableau vide.
        $resolver->setDefaults([]);
    }
}

