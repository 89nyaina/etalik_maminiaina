<?php
namespace App\Tests\Unit;

use App\Entity\InfosVehiculesClients;
use PHPUnit\Framework\TestCase;

class InfosVehiculesClientsTest extends TestCase
{
    public function testSettingCompteAffaire()
    {
        $infos = new InfosVehiculesClients();
        $infos->setCompteAffaire('TestCompte');
        
        $this->assertEquals('TestCompte', $infos->getCompteAffaire());
    }

    // Ajoutez des méthodes similaires pour tester d'autres propriétés...
}
